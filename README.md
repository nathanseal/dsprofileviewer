The dsprofile viewer library
============================

The dsprofile viewer library is an extension of the dsprofile
library (http://bitbucket.org/inkytonik/dsprofile). It provides
a GUI for viewing profiling data so that it is easier to find
relevant information compared to just using the default console
out put of the profiler.

Information about how to download, build and run
================================================

The library can be compiled in the same manner as dsprofile and more info can be found at it�s project page (http://bitbucket.org/inkytonik/dsprofile)
To build just use the Scala simple build tool in the "dsprofileviewer/dsprofile-viewer/" directory.

To use follow the instuctions form the dsprofile readme but with a simple change of when starting a profile just use

	profile (c)

instead of

	profile (c, "attribute", "cached")

because the dimensions are unneeded for the gui. A simple example program that has been profiled can be found in "dsprofileviewer/exampleProgram/" directory.
