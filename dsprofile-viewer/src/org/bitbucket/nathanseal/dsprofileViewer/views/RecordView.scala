/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views
import javafx.fxml.Initializable

import org.bitbucket.nathanseal.dsprofileViewer.model.{ ProfilerInterface, StatisticsRow }
import org.bitbucket.nathanseal.dsprofileViewer.MainController
import javafx.collections.ObservableList
import javafx.collections.FXCollections
import java.net.URL
import java.util.ResourceBundle
import scala.collection.JavaConversions._
import org.bitbucket.inkytonik.dsprofile.Events._
import org.bitbucket.nathanseal.dsprofileViewer.model._

/**
 * trait for views that need to access records
 *
 * most of the views will have this trait but an example of one that dosn't is Profileview
 * 
 * this class works like a controller to glue the model and view together
 */
trait RecordView extends ScalaHelpers {
	type Filter = MainController.profiler.Filter
	def getAllDimensions(): List[String] = {
		MainController.profiler.baseFilter.availableDimensions
	}

	def getRowStatistics(): ObservableList[StatisticsRow] = {
		MainController.profiler.getCurrentStatistics()
	}
	def getRowStatisticsList(): List[StatisticsRow] = {
		List(getCurrentFilter.statistics: _*)
	}
	//def getCurrentFilters: ObservableList[Filter] = MainController.profiler.currentFiltersObs
	def getCurrentFiltersList: List[Filter] = {
		MainController.profiler.getCurrentFilters
	}
	def resetFilters() = MainController.profiler.resetFilters()
	def applyDimension(lvl: Int, dim: Dimension) = MainController.profiler.applyDimension(lvl, dim)
	def applyValue(lvl: Int, v: Option[Value]) = MainController.profiler.applyValue(lvl, v)
	def applyDimension(dim: Dimension) = MainController.profiler.applyDimension(dim)
	def applyValue(v: Option[Value]) = MainController.profiler.applyValue(v)
	def getCurrentFilter() = MainController.profiler.currentFilter
	
	def obsToList[T](obs: ObservableList[T]): List[T] = {
		List(obs: _*)
	}

	//def onFilterChange(): Unit = {}
	def onChange(): Unit = {}
	
	def valueToString(v: Option[Value]): String = {
		v match {
			case None =>
				"None"
			case sv: Some[Value] =>
				if(sv == null)
					"null"
				else
					sv.get.toString()
			case null =>
				""
		}
	}
	
	def profiledTime = MainController.profiler.profiledTime
	def totalTime = MainController.profiler.totalTime
	def profiledPercent = MainController.profiler.percentd(MainController.profiler.profiledTime, MainController.profiler.totalTime)
	def totalRecords = MainController.profiler.baseFilter.currentRecords.size
	def totalDimensions = MainController.profiler.baseFilter.availableDimensions.size
	
	def getExtraDimension = MainController.profiler.extraDimension
	def setExtraDimension(x: Option[Dimension]) = {MainController.profiler.extraDimension = x}
	def getMetric = MainController.profiler.metric
	def setMetric(x: String) = {MainController.profiler.metric = x}

	//attach events to changes
	MainController.profiler.addListener(onChange)
}