/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.layout.AnchorPane
import javafx.scene.layout.Region
import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.fxml.FXML
import scala.collection.mutable.Map
import java.awt.Desktop
import java.net.URI
import javafx.scene.control.RadioMenuItem
import org.bitbucket.nathanseal.dsprofileViewer.MainController


/**
 * class for handling the main window and its basic golbel functionalities such as
 * running the profile, exiting the program and selecting a inner view to use.
 */
class MainWindow extends AnchorPane with BaseView{
	val fxmlFilename = "MainInterface.fxml"
	loadInner()

	@FXML
	var viewingArea: Region = _

	def close(e: ActionEvent): Unit = {
		println("closeing")
		Platform.exit()
	}
	def startProfile(e: ActionEvent): Unit = {
		runProfile()
	}
	/**
	 * interface for a controller to specify how to run the profile
	 */
	var runProfile: () => Unit = () => {}

	def setViewingArea(view: Region) {
		//change the main viewing area to use the given pane
		if (view != viewingArea) {
			AnchorPane.setTopAnchor(view, AnchorPane.getTopAnchor(viewingArea))
			AnchorPane.setLeftAnchor(view, AnchorPane.getLeftAnchor(viewingArea))
			AnchorPane.setRightAnchor(view, AnchorPane.getRightAnchor(viewingArea))
			AnchorPane.setBottomAnchor(view, AnchorPane.getBottomAnchor(viewingArea))
			getChildren().remove(viewingArea)
			getChildren().add(view)
			viewingArea = view
		}
	}
	def resetFilters() {
		viewingArea match {
			case r: RecordView =>
				r.resetFilters()
			case _ =>
		}
	}
	def getViewingArea(): Region = viewingArea
	def openAbout() {
		Desktop.getDesktop().browse(new URI("http://bitbucket.org/nathanseal/dsprofileviewer"));
	}
	
	def setDisplay(e: ActionEvent) {
		if(!viewingArea.isInstanceOf[ProfilingView])
		{
			e.getTarget() match {
				case item: RadioMenuItem =>
					item.getId() match {
						case "pieViewRadio" =>
							setViewingArea(new PieView)
						case "barViewRadio" =>
							setViewingArea(new BarView)
						case _ =>
							setViewingArea(new BasicView)
					}
				case _ =>
			}
		}
	}
}
