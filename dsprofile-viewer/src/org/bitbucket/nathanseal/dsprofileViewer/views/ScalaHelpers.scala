/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.Node
import javafx.scene.Node
import javafx.util.Callback
import javafx.beans.value.ObservableValue
import javafx.beans.property.ReadOnlyObjectWrapper
import javafx.scene.control.{TableCell, TableRow, TableColumn, ListView, ListCell, TableView}
import javafx.scene.control.TableColumn.CellDataFeatures
import javafx.event.Event
import javafx.event.EventHandler
import javafx.collections.ListChangeListener
import javafx.beans._
import java.util.Comparator

trait ScalaHelpers {
		
	/*
	 * some helper functions to wrap java classes with scala functions, because the syntax is much nicer
	 */
	/*
	 * wrap for the callback class
	 */
	def callBack[S, T](func: S => T): Callback[S, T] = {
		new Callback[S, T]() {
			def call(x: S): T = {
				func(x)
			}
		}
	}
	/*
	 * wrap around valueFactory for tableViews 
	 */
	def valueFactory[S, T](func: S => T): Callback[CellDataFeatures[S, T], ObservableValue[T]] = {
		callBack[CellDataFeatures[S, T], ObservableValue[T]]((x: CellDataFeatures[S, T]) => {
			new ReadOnlyObjectWrapper(func(x.getValue()))
		})
	}
	/*
	 * wrap around cellFactory for tableViews 
	 */
	def cellFactory[S, T](func: TableColumn[S, T] => TableCell[S, T]): Callback[TableColumn[S, T], TableCell[S, T]] = {
		callBack[TableColumn[S, T], TableCell[S, T]]((x: TableColumn[S, T]) => {
			func(x)
		})
	}
	/*
	 * wrap around rowFactory for tableViews 
	 */
	def rowFactory[S](func: TableView[S] => TableRow[S]): Callback[TableView[S], TableRow[S]] = {
		callBack[TableView[S], TableRow[S]]((x: TableView[S]) => {
			func(x)
		})
	}
	/*
	 * wrap around cellFactory for list cells 
	 */
	def listCellFactory[T](func: ListView[T] => ListCell[T]): Callback[ListView[T], ListCell[T]] = {
		callBack[ListView[T], ListCell[T]]((x: ListView[T]) => {
			func(x)
		})
	}
	/*
	 * wrap around eventHander
	 */
	def eventHandler[T <: Event](func: T => Unit): javafx.event.EventHandler[T] = {
		new EventHandler[T]() {
			def handle(event: T) {
				func(event)
			}
		}
	}
	
	/**
	 * simple listener for a list
	 * 
	 * E - the element type of the list
	 */
	def listListener[E](func: () => Unit): ListChangeListener[E] = {
		new ListChangeListener[E]() {
			def onChanged(c: ListChangeListener.Change[_ <: E]): Unit = {
				func()
			}
		}
	}
	def Listener[E](func: () => Unit): InvalidationListener = {
		new InvalidationListener() {
			override def invalidated(o : Observable): Unit = {
				func()
			}
		}
	}
	
	def cmp[T](func: (T,T) => Int): Comparator[T] = {
		new Comparator[T] {
			def compare(a: T, b: T): Int = {
				func(a, b)
			}
		}
	}
}

//object for calling the functions with
object ScalaHelpers extends ScalaHelpers