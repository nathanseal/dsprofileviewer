/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.text.Text

class SummaryView extends Text with RecordView {
	onChange()
	override def onChange() {
		setText(("%6d ms total time\n" +
				"%6d ms profiled time\n" +
				"  %.3f%% profiled\n" +
				"%6d profile records\n" + 
				"   with %d dimensions").format(
				totalTime / 1000000,
				profiledTime / 1000000,
				profiledPercent,
				totalRecords,
				totalDimensions
		))
	}
}