/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.layout.HBox
import javafx.scene.control.Label
import javafx.scene.control.Button
import org.bitbucket.inkytonik.dsprofile.Events._

class BreadCrumb extends HBox with BaseView with RecordView {
	val fxmlFilename: String = null
	getStyleClass().add("bread-crumb")
	val label = new Label()
	
	getChildren().clear()
	getChildren().add(label)
	onChange()
	
	override def onChange() {
		getChildren().clear()
		val rootButton = new BreadCrumbButton("/")
		rootButton.setOnAction(eventHandler(e => resetFilters))
		getChildren().add(rootButton)
		var lvl = 0
		for(filter <- getCurrentFiltersList) {
			val currentlvl = lvl;
			val dimButton = new BreadCrumbButton(filter.dimension)
			dimButton.setOnAction(eventHandler(e => applyDimension(currentlvl, filter.dimension)))
			getChildren().add(dimButton)
			if(filter.value != null) {
				val valueButton = filter.value match {
					case Some(s) =>
						new BreadCrumbButton(s.toString)
					case None =>
						new BreadCrumbButton("Dimension not set")
				}
				valueButton.setOnAction(eventHandler(e => applyValue(currentlvl, filter.value)))
				getChildren().add(valueButton)
			}
			lvl += 1
		}
	}
}
/**
 * class for the buttons in the bread crumb bar
 */
class BreadCrumbButton(text: String) extends Button() {
	if(text != null)
		setText(text.toString)
	else
		setText("null")
	setMaxWidth(100)
}