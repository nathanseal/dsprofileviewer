/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import java.io.File
import java.net.URL
import javafx.scene.layout.Pane
import javafx.scene.Node
import javafx.fxml.FXMLLoader
import javafx.util.Callback
import javafx.beans.value.ObservableValue
import javafx.beans.property.ReadOnlyObjectWrapper
import javafx.scene.control.{TableCell, TableRow, TableColumn, ListView, ListCell, TableView}
import javafx.scene.control.TableColumn.CellDataFeatures
import javafx.event.Event
import javafx.event.EventHandler
import javafx.collections.ListChangeListener
import java.util.ResourceBundle
import javafx.fxml.Initializable


/**
 * trait for Panes that use fxml for their inner contents
 */
trait BaseView extends Pane with Initializable with ScalaHelpers {
	val fxmlFilename: String
	def loadInner() {
		println("loadInner " + fxmlFilename)
		if(fxmlFilename != null) {
			val location:URL = classOf[BaseView].getResource(fxmlFilename)
			val fxmlLoader: FXMLLoader = new FXMLLoader(location)
			fxmlLoader.setRoot(this)
			fxmlLoader.setController(this)
			
			fxmlLoader.load()
		}
	}
	override def initialize(location: URL, resources: ResourceBundle) {
		start()
	}
	def start() {}
}