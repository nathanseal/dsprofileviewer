/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.layout.VBox
import javafx.scene.control._
import javafx.fxml.FXML
import javafx.event.Event
import org.bitbucket.inkytonik.dsprofile.Events._

class ComboBoxFilterer extends VBox with RecordView with BaseView {
	val fxmlFilename = null
	getChildren().clear()
	onChange()
	override def onChange() {
		/*
		 * look thought the filters and update the combo boxes to match
		 */
		var lvl = 0
		var dimBoxes = List[FilterCombo[Dimension]]()
		var valueBoxes = List[FilterCombo[Option[Value]]]()
		getChildren().clear()

		if(getAllDimensions.length > 0) {
			var firstCombo = new FilterCombo[Dimension]("Select Dimension")
			firstCombo.getItems().addAll(getAllDimensions : _*)
			getChildren().add(firstCombo)
			dimBoxes ::= firstCombo
			for(f <- getCurrentFiltersList) {
				dimBoxes.head.setVal(f.dimension)
				if(f.availableValues.size > 0) {
					val valCombo = new FilterCombo[Option[Value]]()
					valCombo.getItems().addAll(f.availableValues : _*)
					getChildren().add(valCombo)
					valueBoxes ::= valCombo
					if(f.value != null) {
						valCombo.setVal(f.value)
						if(f.availableDimensions.size > 0) {
							val dimCombo = new FilterCombo[Dimension]("Select Dimension")
							dimCombo.getItems().addAll(f.availableDimensions  : _*)
							getChildren().add(dimCombo)
							dimBoxes ::= dimCombo
						}
					}
				}
			}
		}
		
		dimBoxes = dimBoxes.reverse
		valueBoxes = valueBoxes.reverse
		
		//set the actions on the combo boxes
		lvl = 0
		for(x <- dimBoxes) {
			val currentlvl = lvl;
			x.setOnAction(eventHandler(e => {
				applyDimension(currentlvl, x.getValue())
				}))
			lvl += 1
		}
		lvl = 0
		for(x <- valueBoxes) {
			val currentlvl = lvl;
			x.setOnAction(eventHandler(e => {
				applyValue(currentlvl, x.getValue()
				)}))
			lvl += 1
		}
	}
}

class FilterComboCell[T] extends ListCell[T] {
	override def updateItem(item: T, empty: Boolean) {
		super.updateItem(item, empty)
		if(empty) {
			setText("")
		}
		else {
			if(item == None) {
				null
			}
 			val text = item match {
				case o : Option[Value] =>
					o match {
						case Some(s) =>
							s.toString
						case None =>
							"Not set"
					}
				case _ =>
					item.toString
			}
			if(text.size > 30) {
				val tip = new Tooltip(text)
				tip.setWrapText(true)
				tip.setPrefWidth(300)
				this.setTooltip(tip)
				setText(text.substring(0, 27) + "...")
			}
			else {
				setText(text)
			}
		}
	}
}

class FilterCombo[T](PromptText: String = "Select Value") extends ComboBox[T] {
	//fill the space
	setMaxWidth(99999)
	def setCell() {
		setCellFactory(ScalaHelpers.callBack((x:ListView[T]) => {
				val result = new FilterComboCell[T]()
				result
			})
		)
	}
	setCell()
	setPromptText(PromptText)
	setButtonCell(new FilterComboCell[T]())
	def setVal(v: T) {
		setValue(v)
		setCell()
	}
}
