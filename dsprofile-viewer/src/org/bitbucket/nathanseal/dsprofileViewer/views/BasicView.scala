/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.layout.AnchorPane
import javafx.event.ActionEvent
import javafx.scene.control._
import javafx.fxml.FXML
import javafx.fxml.Initializable
import org.bitbucket.nathanseal.dsprofileViewer.model.StatisticsRow
import java.net.URL
import java.util.ResourceBundle
import javafx.scene.control.cell.PropertyValueFactory
import javafx.beans.property.SimpleListProperty
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn.CellDataFeatures
import javafx.scene.control.PopupControl
import java.util.Comparator
import javafx.scene.layout.VBox
import javafx.collections.FXCollections
import scala.collection.JavaConversions._
import org.bitbucket.inkytonik.dsprofile.Events._
import javafx.event.Event
import javafx.event.EventHandler
import javafx.util.Callback

/**
 * a basic view of the profile data that displays it in a table like in the original dsprofile libaray 
 */
class BasicView extends AnchorPane with BaseView with RecordView {
	val fxmlFilename: String = "BasicView.fxml"
	loadInner()

	
	@FXML
	var table: TableView[StatisticsRow] = _
	@FXML
	var SelecterBox: VBox = _
	@FXML
	var valueColumn: TableColumn[StatisticsRow, Option[Value]] = _
	@FXML
	var countpColumn, totalpColumn, selfpColumn, descpColumn: TableColumn[StatisticsRow, Double] = _
	@FXML
	var totalmsColumn, selfmsColumn, descmsColumn, countColumn: TableColumn[StatisticsRow, Long] = _
	//@FXML
	//var breadcrumbLabel: Label = _
	
	override def start() {
		totalmsColumn.setCellValueFactory(valueFactory(_.getTotalms))
		selfmsColumn.setCellValueFactory(valueFactory(_.getSelfms))
		descmsColumn.setCellValueFactory(valueFactory(_.getDescms))
		countColumn.setCellValueFactory(valueFactory(_.getCount))
		
		totalpColumn.setCellValueFactory(valueFactory(_.getTotalp))
		totalpColumn.setCellFactory(cellFactory(x => new PercentCell()))
		selfpColumn.setCellValueFactory(valueFactory(_.getSelfp))
		selfpColumn.setCellFactory(cellFactory(x => new PercentCell()))
		descpColumn.setCellValueFactory(valueFactory(_.getDescp))
		descpColumn.setCellFactory(cellFactory(x => new PercentCell()))
		countpColumn.setCellValueFactory(valueFactory(_.getCountp))
		countpColumn.setCellFactory(cellFactory(x => new PercentCell()))
		
		valueColumn.setCellFactory(cellFactory(x => new ValueCell()))
		valueColumn.setCellValueFactory(valueFactory(_.getValue))
		
		//sneaky of getting a Comparator that will sort nicely
		//the default Comparator for a TableColumn[StatisticsRow, Value] is good because
		//it will sort the values when posible
		val valueComp = (new TableColumn[StatisticsRow, Value]).getComparator()
		val optionValueComp = valueColumn.getComparator()
		valueColumn.setComparator(new Comparator[Option[Value]] {
			override def compare(one: Option[Value], two: Option[Value]): Int = {
				//one compare two
				(one, two) match {
					case (Some(a), Some(b)) =>
						valueComp.compare(a, b)
					case _ =>
						optionValueComp.compare(one, two)
				}
			}
		})
		
		table.setRowFactory(rowFactory(x => new ClickableRow()))
		table.setItems(getRowStatistics())
	}
	
	override def onChange() {
		table.setItems(getRowStatistics())
	}
}

/**
 * class for the value table cell to give it a tooltip
 */
class ValueCell extends TableCell[StatisticsRow, Option[Value]] {
	override def updateItem(item: Option[Value], empty: Boolean) {
		super.updateItem(item, empty)

		if(!empty) {
			val text = item match {
				case Some(s) =>
					if(s == null)
						"null"
					else
						s.toString
				case None =>
					"Dimension not set"
			}
			if(text.length() > 30) {
				val tip = new Tooltip(text)
				tip.setWrapText(true)
				tip.setPrefWidth(300)
				this.setTooltip(tip)
			}
			setText(text)
		}
	}
}
class PercentCell extends TableCell[StatisticsRow, Double] {
	override def updateItem(item: Double, empty: Boolean) {
		super.updateItem(item, empty)
		if(empty) {
			setText("")
		}
		else {
			setText("%.3f".format(item))
		}
	}
}

class ClickableRow extends TableRow[StatisticsRow] with ScalaHelpers with RecordView {
	setOnMouseClicked(eventHandler(x => {
		if(x.getClickCount() == 2) {
			if(getItem != null && getItem.getValue != null) {
				applyValue(getItem.getValue)
			}
		}
	}))
	def onReset = null
}
