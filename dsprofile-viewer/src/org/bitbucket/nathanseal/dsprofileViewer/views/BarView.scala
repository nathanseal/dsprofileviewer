/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.layout.AnchorPane
import javafx.fxml.Initializable
import java.net.URL
import java.util.ResourceBundle
import javafx.scene.chart._
import javafx.scene.control.ComboBox
import javafx.fxml.FXML
import javafx.collections.ObservableList
import javafx.collections.FXCollections
import javafx.fxml.Initializable
import javafx.geometry.Side
import org.bitbucket.nathanseal.dsprofileViewer.model.StatisticsRow
import org.bitbucket.inkytonik.dsprofile.Events._
import javafx.scene.layout.VBox
import scala.collection.JavaConversions._

class BarView extends AnchorPane with BaseView with RecordView {
	val fxmlFilename: String = "BarView.fxml"
	loadInner()

	@FXML
	var myBarChart: XYChart[String, Double] = _
	@FXML
	var metricCombo: ComboBox[String] = _
	@FXML
	var leftvbox: VBox = _

	var secondDimCombo: FilterCombo[Dimension] = _

	override def start() {
		myBarChart.setLegendSide(Side.BOTTOM)
		metricCombo.getItems().setAll("Count", "Total ms", "Self ms", "Desc ms")
		metricCombo.setValue(getMetric)
		metricCombo.valueProperty().addListener(ScalaHelpers.Listener[String](() => {
			setMetric(metricCombo.getValue())
			myBarChart.setTitle(getMetric)
			myBarChart.getYAxis.setLabel(getMetric)
			updateChart()
		}))
		myBarChart.setTitle(getMetric)
		myBarChart.getYAxis.setLabel(getMetric)
		secondDimCombo = new FilterCombo[Dimension]("Select Dimension")
		leftvbox.getChildren().add(secondDimCombo)
		secondDimCombo.valueProperty().addListener(ScalaHelpers.Listener[String](() => {
			setExtraDimension(secondDimCombo.getValue() match {
				case null =>
					None
				case _ =>
					Some(secondDimCombo.getValue())
			})
			updateChart()
		}))
		secondDimCombo.getItems().setAll(getCurrentFilter().availableDimensions: _*)
		getExtraDimension match {
			case Some(dim) =>
				secondDimCombo.setValue(dim)
			case _ =>
				secondDimCombo.setValue(null)
		}
		updateChart()
	}
	override def onChange() {
		//remove the second dimension on a change
		secondDimCombo.getItems().setAll(getCurrentFilter().availableDimensions: _*)
		setExtraDimension(None)
		secondDimCombo.setValue(null)
		updateChart()
	}
	def updateChart() {
		val serieslist: List[XYChart.Series[String, Double]] = getExtraDimension match {
			case None =>
				myBarChart.setLegendVisible(false)
				List(makeSeries(getRowStatisticsList))
			case Some(dim) =>
				myBarChart.setLegendVisible(true)
				val last = getCurrentFilter.lastFilter.setDimension(dim)
				last.availableValues.map(v => {
					val fil = last.setValue(v).setDimension(getCurrentFilter.dimension)
					val stats = obsToList(fil.statistics)
					makeSeries(stats, valueToString(v))
			})
		}
		/*
		 * change the items over in a way that the animations stay nice
		 */
		if(myBarChart.getData().size() > serieslist.size) {
			myBarChart.getData().remove(serieslist.size, myBarChart.getData().size())
		}
		for(i <- 0 until myBarChart.getData.size) {
			if(myBarChart.getData.get(i).getData.size == serieslist.get(i).getData.size) {
				for(j <- 0 until serieslist.get(i).getData.size) {
					val from = serieslist.get(i).getData.get(j)
					if(j < myBarChart.getData.get(i).getData.size) {
						val to = myBarChart.getData.get(i).getData.get(j)
						to.setYValue(from.getYValue())
						to.setXValue(from.getXValue())
					}
					else {
						myBarChart.getData.get(i).getData.add(from)
					}
					/*if(j < myBarChart.getData.get(i).getData.size) {
						myBarChart.getData.get(i).getData.get(j).setYValue(serieslist.get(i).getData.get(j).getYValue())
						myBarChart.getData.get(i).getData.get(j).setXValue(serieslist.get(i).getData.get(j).getXValue())
					}*/
				}
			}
			else {
				myBarChart.getData.get(i).getData.setAll(serieslist.get(i).getData())
			}
		}
		for(i <- myBarChart.getData.size until serieslist.size) {
			myBarChart.getData.add(serieslist(i))
		}
		
		//myBarChart.getData().clear()
		//myBarChart.getData().setAll(serieslist: _*)
	}
	
	def makeSeries(stats: List[StatisticsRow], name: String = ""): XYChart.Series[String, Double] = {
		val series: XYChart.Series[String, Double] = new XYChart.Series[String, Double]()
		series.setName(name)
		for (stat <- stats) {
			val label = if (stat.getValueString.size <= 30) {
				stat.getValueString
			} else {
				stat.getValueString.substring(0, 27) + "..."
			}

			if (metricCombo != null) {
				val data = new XYChart.Data(label, getStat(stat))
				series.getData().add(data)
			}
		}
		series
	}
	def getStat(row: StatisticsRow): Double = {
		getMetric match {
			case "Total ms" =>
				row.getTotalms
			case "Self ms" =>
				row.getSelfms
			case "Desc ms" =>
				row.getDescms
			case _ =>
				//count or other
				row.getCount
		}
	}
}