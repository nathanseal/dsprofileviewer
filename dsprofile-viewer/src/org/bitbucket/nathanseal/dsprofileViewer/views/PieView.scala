/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.views

import javafx.scene.layout.AnchorPane
import javafx.fxml.Initializable
import java.net.URL
import java.util.ResourceBundle
import javafx.scene.chart._
import javafx.scene.control.ComboBox
import javafx.fxml.FXML
import javafx.collections.ObservableList
import javafx.collections.FXCollections
import javafx.fxml.Initializable
import javafx.geometry.Side
import org.bitbucket.nathanseal.dsprofileViewer.model.StatisticsRow

class PieView extends AnchorPane with BaseView with RecordView {
	val fxmlFilename: String = "PieView.fxml"
	var dataMap = Map[StatisticsRow, PieChart.Data]()
	loadInner()
	
	@FXML
	var pieChart: PieChart = _
	
	@FXML
	var metricCombo: ComboBox[String] = _
	
	
	override def start() {
		pieChart.setLabelsVisible(true)
		pieChart.setLegendSide(Side.BOTTOM)
		pieChart.setLegendVisible(true)
		metricCombo.getItems().setAll("Count", "Total ms", "Self ms", "Desc ms")
		metricCombo.setValue(getMetric)
		metricCombo.valueProperty().addListener(ScalaHelpers.Listener[String](() => {
			setMetric(metricCombo.getValue())
			pieChart.setTitle(getMetric)
			for(stat <- getRowStatisticsList) {
				dataMap.get(stat) match {
					case Some(s) =>
						s.setPieValue(getStat(stat))
					case None =>
						throw new Exception("value should be set")
				}
			}
		}))
		onChange()
	}
	def getStat(row: StatisticsRow): Double = {
		getMetric match {
			case "Total ms" =>
				row.getTotalms
			case "Self ms" =>
				row.getSelfms
			case "Desc ms" =>
				row.getDescms
			case _ =>
				//count or other
				row.getCount
		}
	}
	override def onChange() {
		val pieChartData: ObservableList[PieChart.Data] = FXCollections.observableArrayList()
		for(stat <- getRowStatisticsList) {
			val label = if(stat.getValueString.size <= 30) {
				stat.getValueString
			} else {
				stat.getValueString.substring(0, 27) + "..."
			}
			
			val dataItem =  new PieChart.Data(label,  getStat(stat))
			dataMap += (stat -> dataItem)
			pieChartData.add(dataItem)
		}
		pieChart.setData(pieChartData)
	}
}