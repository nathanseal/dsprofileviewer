/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer

import javafx.application.Application
import javafx.stage.Stage
import javafx.scene.Scene
import org.bitbucket.nathanseal.dsprofileViewer.model.ProfilerInterface
import org.bitbucket.nathanseal.dsprofileViewer.views._
import scala.collection.JavaConversions._
//import com.nathanseal.dsprofileViewer.views.MainWindow

/**
 * the application class for the program
 */
class MainController extends Application {
	var mainWindow: MainWindow = _
	
	override def start(primaryStage: Stage): Unit = {
		mainWindow = new MainWindow()
		mainWindow.setViewingArea(new BasicView())
		mainWindow.runProfile = this.runProfile
		var scene = new Scene(mainWindow)
		primaryStage.setScene(scene)
		scene.getStylesheets().add("styles.css");
		primaryStage.setOnCloseRequest(ScalaHelpers.eventHandler(e => {
			MainController.profiler.killThread
		}))

		primaryStage.setTitle("Profile Viewer")
		primaryStage.show()
		runProfile()
	}
	
	/**
	 * run the profile to get records to show
	 */
	def runProfile(): Unit = {
		val oldView = mainWindow.getViewingArea()
		mainWindow.setViewingArea(new ProfilingView())
		println("start profileing")
		MainController.profiler.runProfile(ProfileFinished)
		
		/**
		 * callback for when the profiling is finished
		 */
		def ProfileFinished() {
			println("end profileing")
			//nested to access oldView
			mainWindow.setViewingArea(oldView)
			mainWindow.resetFilters()
		}
	}
}

object MainController {
	val profiler = new ProfilerInterface()
	
	//the result of the computation
	def result = profiler.result
	
	//the setter wraps the computation to save the result in result instead of returning it
	def setComputation(comp:() => Any): Unit = {
		profiler.computation = comp
	}
	
	def runApplication() {
		Application.launch(classOf[MainController])
	}
}