/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer


import org.bitbucket.inkytonik.dsprofile.Profiler
import org.bitbucket.nathanseal.dsprofileViewer.MainController

trait ProfileViewer extends Profiler {
	/**
	 * the main entry point of the gui profiler made to look like the standard profiler so not much needs to change
	 * 
	 * open the gui application
	 */
	
	override def profile[T](computation :  => T, dimensionNames : org.bitbucket.inkytonik.dsprofile.Events.Dimension*) : T = {
		//set the computation
		MainController.setComputation(() => computation)
		
		//run the gui application
		MainController.runApplication()
		
		//return the last result calculated if the computation is run multiple times
		MainController.result.asInstanceOf[T]
	}
}

/**
 * default profile viewer
 */
object ProfileViewer extends ProfileViewer
