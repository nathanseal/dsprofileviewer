/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.model

import org.bitbucket.inkytonik.dsprofile._
import scala.collection.immutable.List
import javafx.collections.ObservableList
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans._
import scala.collection.JavaConversions._
import javafx.collections._
import java.util.IdentityHashMap
import Events._
import scala.collection.mutable.{ HashMap, HashSet, ListBuffer }
import java.lang.System.nanoTime
import javafx.concurrent.Task
import javafx.event.EventHandler
import javafx.concurrent.WorkerStateEvent



/**
 * class for extensions on the profiler so that it can keep track of a state of what is being looked at
 * and also provide some observable interfaces for connecting to ui objects
 */
class ProfilerInterface extends Profiler {
	var currentFilter: Filter = new Filter()
	var baseFilter: Filter = new BaseFilter()
	
	var extraDimension: Option[Dimension] = None
	var metric: String = "Count"
	
	var totalTime: Long = 0l
	var profiledTime: Long = 0l
	var computation: () => Any = null
	var result: Any = null

	private[this] var Profiling: Boolean = false
	def isProfiling = Profiling
	private[this] var thread: Thread = null
	
	//this is a observable is used to handle listeners to the this model
	private[this] val observable = new SimpleBooleanProperty()
	
	/**
	 * run the profile again
	 */
	def runProfile(callback: () => Unit) {
		if(!Profiling) {
			Profiling = true
			val myTask = new Task[Unit] {
				override def call() {
					result = profile(computation())
					var l: List[Int] = null
					
				}
			}
			val e = new EventHandler[WorkerStateEvent] {
				override def handle(e: WorkerStateEvent) {
					callback()
					Profiling = false
					thread = null
				}
			}
			val fail = new EventHandler[WorkerStateEvent] {
				override def handle(e: WorkerStateEvent) {
					callback()
					Profiling = false
					println("Error Profiling, Printing stack trace...")
					myTask.getException().printStackTrace()
					thread = null
				}
			}
			myTask.setOnSucceeded(e)
			myTask.setOnFailed(fail)
			thread = new Thread(myTask)
			thread.start()
		}
	}
	
	def killThread() {
		if(thread != null) {
			thread.synchronized {
				if(thread != null) {
					thread.interrupt()
					thread = null
				}
			}
		}
	}
	
	def addListener(func: () => Unit) {
		observable.addListener(new InvalidationListener() {
			override def invalidated(o : Observable): Unit = {
				func()
			}
		})
	}
	def notifyListeners() {
		//simple hack to notify all listeners
		observable.set(!observable.get())
	}
	def applyDimension(lvl: Int, dim: Dimension): Unit = {
		currentFilter = currentFilter.removeFilters(lvl-1).setDimension(dim)
		notifyListeners()
	}
	def applyDimension(dim: Dimension): Unit = {
		currentFilter = currentFilter.setDimension(dim)
		notifyListeners()
	}
	def applyValue(lvl: Int, value: Option[Value]): Unit = {
		currentFilter = currentFilter.removeFilters(lvl).setValue(value)
		notifyListeners()
	}
	def applyValue(value: Option[Value]): Unit = {
		currentFilter = currentFilter.setValue(value)
		notifyListeners()
	}
	def resetFilters(): Unit = {
		currentFilter = baseFilter
		notifyListeners()
	}
	def getCurrentFilters(): List[Filter] = {
		currentFilter match {
			case bf: BaseFilter =>
				List[Filter]()
			case f: Filter=>
				f.previousFilters :+ f
		}
	}
	
	/**
	 * override the print reports function to capture the output from the profile and stop the printing of the reports
	 */
	override def printReports(totalTime: Long, dimensionNames: Seq[Dimension], records: List[Record]) {
		
		//the following can be used to test outputs with the text profiler
		//val allDimensions = records.map(_.dimensions.keys.toList).reduce(_ ::: _).distinct
		//super.printReports(totalTime, Seq("attribute") , records)
		
		//call a different function just so it has a nicer name
		DoneProfiling(totalTime, dimensionNames, records)
	}
	/**
	 * save the results of a profile
	 */
	def DoneProfiling(totalTime: Long, dimensionNames: Seq[Dimension], records: List[Record]) {
		this.totalTime = totalTime
		profiledTime = records.foldLeft (0L) (_ + _.stime)
		//get all the dimension names
		val allDimensions = records.map(_.dimensions.keys.toList).reduce(_ ::: _).distinct
		
		baseFilter = new BaseFilter(records, allDimensions)
		currentFilter = baseFilter
		notifyListeners()
	}
	
	def getCurrentStatistics(): ObservableList[StatisticsRow] = {
		currentFilter.statistics
	}
	
	def getStatisticsForFilter(filter: Filter): ObservableList[StatisticsRow] = {
		
		val result = FXCollections.observableArrayList[StatisticsRow]()
		
		val buckets = if(filter.value == null) {
			val currentDimension: Dimension = filter.dimension
			aggregate(filter.currentRecords, filter.dimension)
		}
		else {
			aggregateTotals(filter.currentRecords)
		}
		
		for((value, ad) <- buckets) {
			val totalms = nanoToMs(ad.time)
			val totalp =  percentd(ad.time, profiledTime)
			val selfms = nanoToMs(ad.stime)
			val selfp = percentd(ad.stime, profiledTime)
			val descms = nanoToMs(ad.dtime)
			val descp = percentd(ad.dtime, profiledTime)
			val count = ad.nrecords
			val countp = percentd(ad.nrecords, filter.currentRecords.size)
			result.add(new StatisticsRow(totalms, totalp, selfms, selfp, descms, descp, count, countp, value))
		}
		result
	}
	
	// calculate a percentage
    def percentd (v : Long, total : Long) : Double = {
        if (total == 0L)
            100.0
        else
            v * 100.0 / total
    }
	
	
	/*
	 * the following code is all copied out of org.bitbucket.inkytonik.dsprofile.Profiler
	 * (with some small modifications)
	 * 
	 */
	
	class DimData {
        /**
         * The evaluations that are summarised here
         */
        val records = new ListBuffer[Record] ()

        /**
         * Total number of records associated with this value
         */
        var nrecords = 0

        /**
         * The descendant records that are accounted for here. We don't really 
         */
        var allDescs = new HashSet[Record] ()

        /**
         * The total time in milliseconds of execution that is apportioned to
         * this value. I.e., the sum of the self time and the descendant time.
         */
        def time : Long =
            stime + dtime

        /**
         * The time in millieseconds that belong just to these evaluations,
         * not including any descendant ones.
         */
        var stime = 0L

        /**
         * The time in milliseconds that descendant records took.
         */
        var dtime = 0L

        /**
         * toString for debugging
         */
        override def toString : String =
            records.toString + ", " + nrecords + ", " + time + "," + dtime

    }
    
    /**
     * Bucket of a dimension value and its associated data.
     */
    type Bucket = (Option[Value],DimData)

    /**
     * A list of buckets.
     */
    type Buckets = List[Bucket]

    /**
     * Aggregate the data in the event list according to the given dimension.
     * Return a sorted list of the aggregated data paired with the dimension
     * values.
     */
    def aggregate (records : List[Record], dimension : Dimension) : Buckets = {

        // Map of data per dimension value
        val dimMap = new HashMap[Option[Value],DimData] ()
        
        // Get a dimension map entry for a given key, creating it in
        // the map if it didn't exist already
        def dimEntry (a : Option[Value]) : DimData = {
        	dimMap.getOrElseUpdate (a, new DimData)
        }

        // Get the dimension map entry for a given record, by looking up the
        // dimension value first
        def getDimEntry (r : Record) : DimData =
            dimEntry (r.dimensions.get(dimension))

        // Summarise by proecssing each of the root records and their descendants
        for (r <- records) {

            // Look up this record to get the appropriate bucket entry
            val entry = getDimEntry (r)
            
            // Record this record and its time with in the entry
            entry.records += r
            entry.nrecords += 1
            entry.stime += r.stime

            // Take into account the descendants. We group them into two groups,
            // those that are in the same bucket entry as r (and hence don't
            // count as descendant time) and those that aren't, which do. The
            // latter's self time is accumulated in the descendant time of the
            // entry. Avoid adding a descendant more than once if it is 
            // encountered.
            for (d <- r.allDescs) {
                val dentry = getDimEntry (d)
                if ((entry != dentry) && (! (entry.allDescs contains d))) {
                    entry.dtime += d.stime
                    entry.allDescs.add (d)
                }
            }

        }

        // Sort the data for in decreasing order of time expended. Ignore zero
        // count ones, these are the attributes that demanded the ones we are
        // interested in but are not included in this aggregation 
        dimMap.toList.filter {
            _._2.nrecords != 0
        }.sortWith {
            _._2.time > _._2.time
        }
        
    }
    
    
    def aggregateTotals (records : List[Record]) : Buckets = {
    	val entry = new DimData
        // Summarise by proecssing each of the root records and their descendants
        for (r <- records) {
            
            // Record this record and its time with in the entry
            entry.records += r
            entry.nrecords += 1
            entry.stime += r.stime
            
            for (d <- r.allDescs) {
                if ((! (records contains d))) {
                    entry.dtime += d.stime
                    entry.allDescs.add (d)
                }
            }
        }
    	List[Bucket]((null,entry))
    }
    
    
    
    /*
     * Filter class, would be nice if it could go in a different file
     * but then it can't have access to the Record type
     */
	class Filter(dim: Dimension = null,
			cr: List[Record] = List[Record](),
			v: Option[Value] = null,
			av: List[Option[Value]] = List[Option[Value]](),
			ad: List[Dimension] = List[Dimension](),
			pf: List[Filter] = List[Filter]()) {
		
		//any records that this filter has removed
		val currentRecords: List[Record] = cr
	
		//the dimension this filter is on
		val dimension = dim
		
	
		/* the value to keep
		 * 
		 * if value is None then the value is that the dimension is not set
		 * else if value is null then no value is selected
		 * else if value is Some then the internal value of the Some is the value to filter by
		 */
		val value: Option[Value] = v
	
		//a list of values that could be picked for this filter
		val availableValues: List[Option[Value]] = av
	
		//a list of what dimensions are left
		val availableDimensions: List[Dimension] = ad
	
		
		val previousFilters: List[Filter] = pf
		
		val statistics = getStatisticsForFilter(this)
		
		//simple string representation of this filter
		override def toString = dimension + " -> " + value
		
		def fullFilter: Boolean = value.isDefined
		
		def lastFilter(): Filter = {
			if(previousFilters.size == 0)
				baseFilter
			else
				previousFilters.last
		}
		
		def getLvl = previousFilters.size
		
		def unfilterdRecords(): List[Record] = {
			if(previousFilters.size > 0) {
				previousFilters.last.currentRecords
			}
			else {
				baseFilter.currentRecords
			}
		}
		
		/**
		 * return the n'th filter
		 */
		def removeFilters(n: Int): Filter = {
			if(n >= 0 && n < previousFilters.size) {
				previousFilters(n)
			}
			else if(n >= previousFilters.size) {
				this
			}
			else {
				baseFilter
			}
		}
		
		def setValue(newval: Option[Value]): Filter = {
			val cr = unfilterdRecords.filter(_.dimensions.get(dimension) == newval)
			val ad = availableDimensions.filterNot(_ == dimension)
			
			
			new Filter(dimension, cr, newval, availableValues, ad, previousFilters)
		}
		
		def setDimension(dim: Dimension): Filter = {
			val av = currentRecords.map(_.dimensions.get(dim)).distinct
			
			if(value != null)
				new Filter(dim, currentRecords, null, av, availableDimensions, previousFilters :+ this)
			else
				this
		}
	}
	//a filter to use for the first one
	class BaseFilter(cr: List[Record] = List[Record](), ad: List[Dimension] = List[Dimension]())
		extends Filter(null, cr, None, List[Option[Value]](), ad, List[Filter]()) {
		
		override def fullFilter: Boolean = true
		
		override def setDimension(dim: Dimension): Filter = {
			val av = currentRecords.map(_.dimensions.get(dim)).distinct
			
			new Filter(dim, currentRecords, null, av, availableDimensions, previousFilters)
		}
	}
    
}
