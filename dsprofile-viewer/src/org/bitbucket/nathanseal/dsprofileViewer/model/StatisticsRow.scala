/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.dsprofileViewer.model
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import org.bitbucket.inkytonik.dsprofile.Events._

/**
 * class for holding a row of profile statistics
 */
class StatisticsRow(ntotalms: Long, ntotalp: Double, nselfms: Long, nselfp: Double, ndescms: Long, ndescp: Double, ncount: Long, ncountp: Double, nvalue: Option[Value]) {
	private val totalms = new SimpleLongProperty(ntotalms)
	private val totalp = new SimpleDoubleProperty(ntotalp)
	private val selfms = new SimpleLongProperty(nselfms)
	private val selfp = new SimpleDoubleProperty(nselfp)
	private val descms = new SimpleLongProperty(ndescms)
	private val descp = new SimpleDoubleProperty(ndescp)
	private val count = new SimpleLongProperty(ncount)
	private val countp = new SimpleDoubleProperty(ncountp)
	private val value = new SimpleObjectProperty[Option[Value]](nvalue)
	
	def getTotalms() = totalms.get()
	def setTotalms(ntotalms: Long) = totalms.set(ntotalms)
	
	def getTotalp() = totalp.get()
	def setTotalp(ntotalp: Double) = totalp.set(ntotalp)
	
	def getSelfms() = selfms.get()
	def setSelfms(nselfms: Long) = selfms.set(nselfms)
	
	def getSelfp() = selfp.get()
	def setSelfp(nselfp: Double) = selfp.set(nselfp)
	
	def getDescms() = descms.get()
	def setDescms(ndescms: Long) = descms.set(ndescms)
	
	def getDescp() = descp.get()
	def setDescp(ndescp: Double) = descp.set(ndescp)
	
	def getCount() = count.get()
	def setCount(ncount: Long) = count.set(ncount)
	
	def getCountp() = countp.get()
	def setCountp(ncountp: Double) = countp.set(ncountp)
	
	def getValue() = value.get()
	def setValue(nvalue: Option[Value]) = value.set(nvalue)
	
	def getValueString(): String = {
		value.get() match {
			case Some(s) =>
				if(s == null)
					"null"
				else
					s.toString
			case None =>
				"Dimension not set"
			case null =>
				""
		}
	}
}