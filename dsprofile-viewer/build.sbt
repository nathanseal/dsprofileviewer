name := "dsprofile-viewer"

version := "0.1"

organization := "comp.mq.edu.au"

// Scala compiler settings

scalaVersion := "2.10.0-RC1"

//scalacOptions ++= Seq ("-deprecation", "-unchecked")
scalacOptions ++= Seq ("-deprecation", "-feature", "-unchecked")

// Interactive settings

logLevel := Level.Info

shellPrompt <<= (name, version) { (n, v) => _ => n + " " + v + "> " }

// Fork the run so that we don't interfere with sbt's JVM

fork in run := true

// Execution

parallelExecution in Test := false

// Dependencies

libraryDependencies ++=
    Seq (
        "org.bitbucket.inkytonik.dsprofile" %% "dsprofile" % "0.1.0"
    )


unmanagedJars in Compile +=
    Attributed.blank (file (System.getProperty ("java.home") + "/lib/jfxrt.jar"))

resolvers += "Sonatype OSS Snapshots Repository" at
    "https://oss.sonatype.org/content/repositories/snapshots"

// Source code locations

// Specify how to find source and test files.  Main sources are
//    - in src directory
//    - all .scala files, except
// Test sources, which are
//    - files whose names end in Tests.scala, which are actual test sources

scalaSource <<= baseDirectory { _ / "src" }

unmanagedSources in Test <<= scalaSource map { s => {
    (s ** "*Tests.scala").get
}}

unmanagedSources in Compile <<= (scalaSource, unmanagedSources in Test) map { (s, tests) =>
    ((s ** "*.scala") --- tests).get
}

// Resources

resourceDirectory in Compile <<= scalaSource
