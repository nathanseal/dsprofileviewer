/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.exampleProgram

import org.bitbucket.inkytonik.dsprofile.Profiler
import org.bitbucket.nathanseal.dsprofileViewer.ProfileViewer
import java.lang.Thread

/**
 * example of opening the profile program
 */
object ExampleProfile {
	def main(args: Array[String]) {
		
		//standard profile
		//Profiler     .profile({ExampleProgram.main(Array[String]())}, "Type")
		
		//gui profile (dosn't need dimensions as they can be chosen with the gui)
		println("should start now")
		ProfileViewer.profile({ExampleProgram.main(Array[String]())})
		println("should be done")
	}
}