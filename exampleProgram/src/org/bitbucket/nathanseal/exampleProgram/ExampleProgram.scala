/**
 * This file is part of dsprofileviewer.
 *
 * Copyright (C) 2012 Nathan Seal.
 *
 * dsprofileviewer is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * dsprofileviewer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dsprofileviewer.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.nathanseal.exampleProgram

import org.bitbucket.inkytonik.dsprofile.Events
import java.lang.Thread

/**
 * This is an example program to create fake profiling data
 */
object ExampleProgram {
	def main(args: Array[String]) {
		Thread.sleep(500)
		Events.start("Type" -> "main")
		Events.start("Type" -> "some very long value for testing gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f")
		Events.finish("Type" -> "some very long value for testing gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f gsdf g sdf g fdsgs dfg fsd g f")
		println("fib " + fib(10))

		println(avg(34, 45, 56, 67, 78, 89, 23, 56, 56, 23, 768, 43534, 435, 76, -45367, 43, 76))
		wait(1)

		Events.finish("Type" -> "main")
	}

	/**
	 * Find the n'th fibonacci number in a way that will make lots of events
	 */
	def fib(n: Int): Int = {
		Events.start("Type" -> "fib", "n" -> n)
		var result = 1
		if (n != 0 && n != 1) {
			result = fib(n - 1) + fib(n - 2)
		}
		Events.finish("Type" -> "fib", "n" -> n)
		result
	}

	/**
	 * Find the mean out out a number of values
	 */
	def avg(nums: Int*): Int = {
		Events.start("Type" -> "avg", "length" -> nums.length)
		var sum: Int = 0
		for (num <- nums) {
			Events.start("Type" -> "inloop")
			sum += num
			Events.finish("Type" -> "inloop")
		}
		Events.finish("Type" -> "avg", "length" -> nums.length)
		sum / nums.length
	}
	
	/**
	 * wait for a bit so the program takes longer
	 */
	def wait(secs: Int) {
		Events.start("Type" -> "sleep")
		Thread.sleep(secs * 1000)
		Events.finish("Type" -> "sleep")
	}
}